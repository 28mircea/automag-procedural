<?php
session_start();
if(!isset($_SESSION['Id'])){
	header("Location: login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AUTOMAG-Pagina my-account</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="wrapper1">
	<div class="logo">
		<a href="index.php"><img src="images/logo.jpg" width="350" height="127" /></a>        
    </div>
    <h1 class="text-logo">AUTOMAG</h1>
  <div class="clear"></div>
  <?php include "includes/header1.php";?>
  <div class="clear"></div>
  <div class="content">
    <div class="column full-width-register1">
     <?php
       echo"<h1>Bine ai venit {$_SESSION['username']}!</h1>";
	   require_once("mysql_connect.php");
	   
	//$query = "SELECT * FROM masina WHERE Id = {$_SESSION['Id']}";
	//$result = mysqli_query($link,$query);
	//if(mysqli_num_rows($result)> 0){
		//while($index = mysqli_fetch_assoc($result)){
			//$_SESSION['Id_masina'] = $index['Id_masina'];
			//}		
	//}
	
	$query = "SELECT m.Id_masina,m.model_masina, m.pret,m.km_parcursi,m.an_fabricatie,p.poza_masina,v.nume,v.telefon FROM masina m INNER JOIN poze p ON m.Id_masina = p.Id_masina INNER JOIN vanzatori v ON m.Id = v.Id WHERE m.Id = {$_SESSION['Id']}" ;
	$result = mysqli_query($link,$query);
	if(mysqli_num_rows($result) > 0)
	{
		    echo"<ul>\n";
			
		  while($masina = mysqli_fetch_assoc($result))		  
		    {
				echo"\t<li>\n"; 
			    $g = $masina['Id_masina']; 
			  
         ?>
         	<div class="container">
        
  <div class="galerie" style="background-image: url('<?php echo"uploads/".$g."/".$masina['poza_masina'];?>'); background-size: 270px 250px;"></div>         
         <?php
		 		      echo"<div id=\"model-masina\">{$masina['model_masina']}</div>\n";	
                      echo"<div id=\"pret-masina\">{$masina['pret']} Euro</div>\n";
					  echo"<div id=\"km-parcursi\">Rulaj: {$masina['km_parcursi']} Km</div>\n";					  
					  echo"<div id=\"data-inmatriculare\">An fabricatie: {$masina['an_fabricatie']}</div>\n";
					  echo"<div id=\"nume-vanzator\">Vanzator: {$masina['nume']}</div>\n";
					  echo"<div id=\"telefon\">Tel.: {$masina['telefon']}</div>\n";
					  echo"<div id=\"buton-edit\">
			          		 	<form action=\"edit.php\" method = \"POST\">
			                 	<input type = \"hidden\" name = \"Id_masina\" value = \"{$masina['Id_masina']}\">
			                 	<input type = \"submit\" value = \"Editeaza\">			  
			                 	</form><br>
								</div>
								<div id=\"buton-delete\">
			                 	<form action=\"delete.php\" method = \"POST\" class=\"del\">
			                 	<input type = \"hidden\" name = \"Id_masina\" value = \"{$masina['Id_masina']}\">
			                 	<input type = \"submit\" value = \"Sterge\">			  
			                 	</form>
			                    </div>\n";
		    }
	     ?> 
 
		    </div><!--inchidere div container-->
		 <?php		
		        echo"</li>";
				
	     ?>       
         <?php
         
		 echo"</ul>";
	  	 
	}
	else{
		  echo "<p>Nu aveti nici un anunt!</p>"; 
	     }
     ?>   
    </div>
  </div>
  <div class="clear"></div>
</div>
<div class="footer1">
&copy;2017 - MIRCEA
</div>
</body>
</html>