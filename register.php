<?php
session_start();
	if(isset($_POST['register'])){
		require_once('mysql_connect.php');	
		$nume = mysqli_real_escape_string($link,$_POST['nume']);
		$telefon = mysqli_real_escape_string($link,$_POST['telefon']);
		$email = mysqli_real_escape_string($link,$_POST['email']);
		$tip_vanzator = mysqli_real_escape_string($link,$_POST['tip_vanzator']);
		$username = mysqli_real_escape_string($link, $_POST['username']);
		$parola = $_POST['parola'];
		$parola1 = $_POST['parola1'];
		
		if((empty($nume)) || (ctype_space($nume))){
			$errors['nume'] = "Trebuie sa mentionati numele";
		}
		if((empty($telefon)) || (!filter_var($telefon,FILTER_VALIDATE_INT))){
			$errors['telefon'] = "Trebuie sa mentionati un numar de telefon";	
		}
		if ((empty($email)) or (!filter_var($email,FILTER_VALIDATE_EMAIL))){
      		 $errors['email'] = "Adresa de email invalida";
        }
		if((isset($tip_vanzator) && ($tip_vanzator == "Dealer"))){
			$tip_vanzator == "Dealer";			
		}
		elseif((isset($tip_vanzator) && ($tip_vanzator == "Persoana_privata"))){
			$tip_vanzator == "Persoana_privata";								 
		}
		else{
			$errors['tip_vanzator'] = "Alegeti una din optiuni";
		}
		if((empty($username)) || (strpos($username," "))){
			$errors['username'] = "Username nu poate fi gol";
		}
		
		//Acum se verifica in BD pentru conturi duplicat de username
		if(!isset($errors)){
		$query = "SELECT * FROM vanzatori WHERE username = '$username'";
		$result = mysqli_query($link,$query);
			if(mysqli_num_rows($result) > 0){
				while($duplicat = mysqli_fetch_assoc($result)){
					if($duplicat['username'] = $username){
						$errors[] = "Username exista deja";	
					}
				}
			}
		}
		if((empty($parola))|| (ctype_space($parola1))){
			$errors['parola'] = "Completati parola";
		}
		if((empty($parola1)) || (ctype_space($parola1))){
			$errors['parola1'] = "Repetati parola";
		}		
		if($parola != $parola1){
		  $errors['parola'] = "Parolele nu sunt identice";	
		}
		//Daca nu sunt erori pana aici se cripteaza parolele inainte de a fi trimise
		if(!isset($errors)){
		   $crypt = password_hash($parola,PASSWORD_BCRYPT);
		}
		//Se adauga utilizatorul in baza de date
		if(!isset($errors)){
		   $query = "INSERT INTO vanzatori VALUES(NULL,'$nume','$telefon','$email','$tip_vanzator','$username','$crypt')";
		   mysqli_query($link,$query);
		   if(mysqli_affected_rows($link) > 0){
			 $succes = "Utilizatorul a fost adaugat";
			 header("Location:login.php");
			}
			else{
				$errors[] = "Utilizatorul nu a fost adaugat";	
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AUTOMAG-Pagina register</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="wrapper">
	<div class="logo">
		<a href="index.php"><img src="images/logo.jpg" width="350" height="127" /></a>        
    </div>
    <h1 class="text-logo">AUTOMAG</h1>
  <div class="clear"></div>
  <?php include "includes/header1.php";?>
  <div class="clear"></div>
  <div class="content">
    <div class="column full-width-register">
  	   <h1>Inregistrare Vanzator</h1>
      <p class="message">Toate campurile marcate cu (*) sunt obligatorii</p>
      <?php
		// afisez erorile sau mesajul de succes
		if (isset($errors)){
  			echo "\t\t<div class=\"error\">\n";
 			 foreach($errors as $error){
   			 echo "\t\t\t<p>$error</p>\n"; 
  			 }
  		echo "\t\t</div>\n";  
       }
       if (isset($succes)){
          echo "\t\t<p class=\"succes\">$succes</p>\n";
      }
	  //if(isset($succes1)){
		//echo "\t\t<div class=\"succes1\">$succes1</div>\n";  
	  //}
      //?> 
       <form action="register.php" method="POST">
       		<fieldset>
            	<legend>Informatii Vanzator</legend><br />
                <table>
                	<tr>
                    	<td><label for="nume" id="nume">Nume*</label></td>
                        <td><input type="text" name="nume" value="" id="nume" /></td>                 
                       
                    </tr>
                    <tr>
                    	<td><label for="telefon" id="telefon1">Telefon*</label></td>
                        <td><input type="text" name="telefon"  value=""   id="telefon1" /></td>
                    </tr> 
                    <tr>
                    	<td><label for="email" id="email">Email*</label></td>
                        <td><input type="text" name="email"  value=""  id="email" /> </td>                     
                     </tr>
                    <tr>
                    	<td><label for="tip_vanzator" id="tip_vanzator">Descriere Vanzator*</label></td>
                        <td>
                        <input type="radio" name="tip_vanzator" value="Dealer">Dealer<br /><br />
        				<input type="radio" name="tip_vanzator" value="Persoana_privata" checked>Persoana Privata<br />		
                        </td>
                      </tr>
                </table>
            </fieldset><br />
            <fieldset><br />
            	<legend>Informatii Logon</legend>
                <table>
                	<tr>
                    	<td><label for="username" id="username">Username*</label></td>
                        <td><input type="text" name="username" value="" id="username" /></td>
                    </tr>
                    <tr>
                    	<td><label for="parola" id="parola">Parola*</label></td>
                        <td><input type="password" name="parola" value="" id="parola" /></td>
                    </tr>
                    <tr>
                    	<td><label for="parola1" id="parola1">Repetati Parola*</label></td>
                        <td><input type="password" name="parola1" value="" id="parola1" /></td>
                    </tr>
                    <tr>
                    	<td><input type="hidden" name="register" value="true"></td>
                        <td><input type="submit" value="Inregistrare" class="shift" /></td>                
                    </tr> 		
                </table>
            </fieldset>
	   </form>
    </div>
  </div>
  <!--<div class="sidebar">
       <!-- Continut sidebar -->

  <!--</div>-->
  <div class="clear"></div>
</div>
<div class="footer">
&copy;2017 - MIRCEA
</div>
</body>
</html>
