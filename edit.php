<?php
session_start();
if(!isset($_SESSION['Id'])){
	header("Location: login.php");
}
if(!isset($_POST['Id_masina'])){
	header("Location: my-account.php");
}

require_once("mysql_connect.php");
$Id_masina = $_POST['Id_masina'];

 $query = "SELECT masina.*,poze.poza_masina FROM masina LEFT JOIN poze ON masina.Id_masina = poze.Id_masina WHERE masina.Id_masina = {$_POST['Id_masina']} AND masina.Id = {$_SESSION['Id']}";
	   $result = mysqli_query($link,$query) or die(mysqli_error($link));

	   while($masina = mysqli_fetch_assoc($result)){
		   $model_masina = $masina['model_masina'];
		   $an_fabricatie = $masina['an_fabricatie'];
		   $km_parcursi = $masina['km_parcursi'];
		   $pret = $masina['pret'];
		   $alte_informatii = $masina['alte_informatii'];
		   $filename = $masina['poza_masina'];
		   $g = $masina['Id_masina'];
		   $desired_dir="uploads/".$g."/";
      }

if(isset($_POST['update'])){ 
	
		$model_masina = trim(mysqli_real_escape_string($link,$_POST['model_masina']));
		$an = $_POST['an'];
		$km_parcursi = mysqli_real_escape_string($link,$_POST['km_parcursi']);
		$pret = mysqli_real_escape_string($link,$_POST['pret']);
		$cutie_viteze = $_POST['cutie_viteze']; 
		
		if(isset($_POST['carte_service'])){
			$carte_service = $_POST['carte_service'];
		}
	    else{
		    $carte_service = "Nu";
	    }
		
		$clasa_emisii = $_POST['clasa_emisii'];
		
		if(isset($_POST['abs'])){
		   $abs = $_POST['abs'];
		}
	    else{
		    $abs = "Nu";
	    }
		
		$alte_informatii = trim(mysqli_real_escape_string($link,$_POST['alte_informatii']));
		
		if (empty($model_masina)){
      		$errors[] = "Modelul masinii nu poate fi gol";
    	}
		
		if (empty($an)){
           $errors[] = "Completati anul de fabricatie!";	  
        }
		
		if ((empty($km_parcursi)) || (!filter_var($km_parcursi, FILTER_VALIDATE_INT))){
      		$errors[] = "Completati numarul de kilometri!";	  
    	}
		
		if ((empty($pret)) || (!filter_var($pret, FILTER_VALIDATE_INT))){
      		$errors[] = "Completati pretul masinii!";	  
    	}
		
		
		if (empty($cutie_viteze)){
           $errors[] = "Alegeti tipul cutiei de viteze!";	  
        }
		
		if (empty($clasa_emisii)){
           $errors[] = "Selectati clsa de emisii!";	  
        }
		
		if(!isset($errors)){
		  
	$query2 = "UPDATE masina SET model_masina='$model_masina',an_fabricatie='$an',km_parcursi='$km_parcursi',pret='$pret',cutie_viteze='$cutie_viteze',carte_service='$carte_service',
	clasa_emisii='$clasa_emisii',abs='$abs',alte_informatii='$alte_informatii' WHERE Id_masina = {$_POST['Id_masina']} AND Id = {$_SESSION['Id']}";
	
				  $result2 = mysqli_query($link,$query2) or die(mysqli_error($link));
				  if(mysqli_affected_rows($link)>=0){
					
					
				//Inlocuire poza
					if ($_FILES['fisier']['error'] == 0){
						$file_name = $_FILES['fisier']['name'];
					    $file_size = $_FILES['fisier']['size'];
					    $file_tmp = $_FILES['fisier']['tmp_name'];
					    $file_type= $_FILES['fisier']['type'];
					   
						$query3 = "DELETE FROM poze WHERE Id_masina = $g"; 
						$result3 = mysqli_query($link,$query3);
							   
					   $query3 = "INSERT INTO poze VALUES (NULL,'$file_name',$g)";
					   $result3 = mysqli_query($link,$query3);
					   
					   $desired_dir="uploads/".$g."/";
					   
					   if(is_dir($desired_dir)){
							if(is_file($desired_dir . "/" . $filename)){
								 unlink($desired_dir . "/" . $filename);
							}
					   }
				
					   if(empty($errors)){
						if(!is_dir($desired_dir)){
							mkdir("uploads/".$g."/", 0700);		
						}
						if(is_dir($desired_dir)){
							move_uploaded_file($file_tmp,"uploads/".$g."/".$file_name);
						}	
					}
				}	
				header("Location:my-account.php");	
				}
				else{
					$errors[] = "Anuntul nu a fost updatat!";	
				}
			}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>AUTOMAG-Pagina edit</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/slider.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="wrapper1">	
  <div class="content">
    <div class="column full-width-register1">
        <?php
		// afisez erorile sau mesajul de succes
		if (isset($errors)){
  			echo "\t\t<div class=\"error\">\n";
 			 foreach($errors as $error){
   			 echo "\t\t\t<p>$error</p>\n"; 
  			 }
  		echo "\t\t</div>\n";  
       }
       if (isset($succes)){
          echo "\t\t<p class=\"succes\">$succes</p>\n";
      }
	  if(isset($succes)){
		echo "\t\t<div class=\"succes1\">$succes1</div>\n";  
	  }
	  if (isset($_SESSION['succes'])){
  		  echo "\t\t<p class=\"succes\">{$_SESSION['succes']}</p>\n";
          unset($_SESSION['succes']);
      }
      ?> 
	   
	   <form action="edit.php" method="POST" enctype= "multipart/form-data">
       		  <fieldset>
            	<legend>Update Informatii Masina</legend><br />
                <table>
                	<tr>
                    	<td><label for="model_masina" id="model_masina">Model Masina</label></td>
                        <td><input type="text" name="model_masina" value="<?php echo $model_masina;?>" id="model_masina" /></td>           
                    </tr>
                    <tr>
                    	<td><label for="An_fabricatie" id="an_fabricatie">Anul de fabricatie</label></td>
                        <td><select name="an" id="an">
                              <option value="" selected="selected">
							   <?php 
							   $query2 = "SELECT an_fabricatie FROM masina WHERE masina.Id = {$_SESSION['Id']} AND masina.Id_masina = {$_POST['Id_masina']}";
							   $result2 = mysqli_query($link,$query2) or die(mysqli_error($link));
							   if($an = mysqli_fetch_assoc($result2)){
							   		echo $an['an_fabricatie'];
							   }
                               ?>
                              </option>
                                <?php                             	
                             	$an = date('Y');								
								while($an >= 1960)
								{
									echo"<option value=\"".$an."\">".$an."</option>\n";
									$an--;
								}
						        ?>
						        ?>
                        	</select>
                        </td>
                    </tr> 
                    <tr>
                    	<td><label for="km_parcursi" id="km_parcursi">Km parcursi</label></td>
                        <td><input type="text" name="km_parcursi" value="<?php echo $km_parcursi;?>" id="km_parcursi" /> </td>                     
                     </tr>
                     <tr>
                    	<td><label for="pret" id="pret">Pret masina(euro)</label></td>
                        <td><input type="text" name="pret" value="<?php echo $pret;?>" id="pret" /> </td>                     
                     </tr>
                     <tr>
                     	<td><label for="cutie_viteze" id="cutie_viteze">Cutie de viteze</label></td>
                        <td>
                        	<select name="cutie_viteze" id="cutie_viteze">
                        		<option value="" selected="selected">                        		
								<?php 
								$query2 = "SELECT cutie_viteze FROM masina WHERE masina.Id = {$_SESSION['Id']} AND masina.Id_masina = {$_POST['Id_masina']}";
							    $result2 = mysqli_query($link,$query2) or die(mysqli_error($link));
							    if($row2 = mysqli_fetch_assoc($result2))
							   		{                                   
									 echo $row2['cutie_viteze'];									 									                     
							    	}																
								?>                                
                                </option>         
                                <option value="Manuala">Manuala</option>
                                <option value="Automata">Automata</option>  
                           </select>
                        </td>
                     </tr>
                     <tr>
                     	<td><label for="carte_service" id="carte_service">Carte de service</label></td>
                        <td>
						<?php 
						$query2 = "SELECT carte_service FROM masina WHERE masina.Id = {$_SESSION['Id']} AND masina.Id_masina = {$_POST['Id_masina']} ";
					    $result2 = mysqli_query($link,$query) or die(mysqli_error($link));
						$row2 = mysqli_fetch_row($result2);
						$carte_service = $row2[6];
						if($carte_service =='Da'){
							echo "<input type=\"checkbox\" name=\"carte_service\" checked=\"checked\" />";
						}
						else{
							echo "<input type=\"checkbox\" name=\"carte_service\" />";
						}
						?>                     
                        </td>
                     </tr>
                     <tr>
                     	<td><label for="clasa_emisii" id="clasa_emisii">Clasa de emisii</label></td>
                        <td>
                        	<select name="clasa_emisii" id="clasa_emisii">
                        		<option value="">Selectati clasa de emisii</option>
								<?php 
									$query2 = "SELECT clasa_emisii FROM masina WHERE masina.Id_masina = {$_POST['Id_masina']} AND masina.Id = {$_SESSION['Id']}";
							        $result2 = mysqli_query($link,$query2) or die(mysqli_error($link));
							        if($clasa = mysqli_fetch_assoc($result2)){
										for ($i = 1; $i <=5; $i++){
											$selected = null;
											if ($clasa['clasa_emisii'] == 'Euro' . $i){
												$selected = "selected";	
											}
											echo "<option value=\"Euro" . $i . "\" $selected>Euro" . $i . "</option>";
										}
							   		}
								?>
                                
          
                        	</select>
                        </td>
                     </tr>
                    <tr>
                    	<td><label for="ABS" id="ABS">ABS</label></td>
                        <td>
                         <?php 
						$query2 = "SELECT carte_service FROM masina WHERE masina.Id = {$_SESSION['Id']} AND masina.Id_masina = {$_POST['Id_masina']}";
					    $result2 = mysqli_query($link,$query) or die(mysqli_error($link));
						$row2 = mysqli_fetch_row($result2);
						$abs = $row2[8];
						if($abs =='Da'){
							echo "<input type=\"checkbox\" name=\"abs\" checked=\"checked\" />";
						}
						else{
							echo "<input type=\"checkbox\" name=\"abs\" />";
						}
						?>  
                        </td>
                    </tr>
                    <tr>
                    	<td><label for="alte_informatii" id="alte_informatii">Alte informatii</label></td>
                        <td><textarea name="alte_informatii" value="" id="alte_informatii"><?php echo $alte_informatii;?></textarea></td><br />						
                    </tr>
                    <tr>      
                           <td><label for="file">Selectati poza &nbsp;(Dim < 1MB)&nbsp;</label></td>
                           <td><input type="file" name="fisier" id="file"/></td>                        	
                    </tr>      
                    <tr>                    
                    	<td colspan="2">
                        	<input type="hidden" name="Id_masina" value="<?php echo $Id_masina; ?>" />
                        	<input type="hidden" name="update" value="true">
                            <input type="submit" value="Update" class="shift" />
                        </td>
                   </tr> 
                </table>
            </fieldset><br />
            </form>
	    </div>
  </div> 
 <div class="clear"></div>
</div>
<div class="footer1">
&copy;2017 - MIRCEA
</div>
</body>
</html>
