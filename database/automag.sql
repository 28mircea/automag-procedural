-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2019 at 07:12 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automag`
--

-- --------------------------------------------------------

--
-- Table structure for table `masina`
--

CREATE TABLE `masina` (
  `Id_masina` int(5) NOT NULL,
  `model_masina` varchar(255) DEFAULT NULL,
  `an_fabricatie` int(4) DEFAULT NULL,
  `km_parcursi` int(6) DEFAULT NULL,
  `pret` int(6) DEFAULT NULL,
  `cutie_viteze` enum('Manuala','Automata') DEFAULT NULL,
  `carte_service` enum('Da','Nu') DEFAULT NULL,
  `clasa_emisii` enum('Euro1','Euro2','Euro3','Euro4','Euro5','Euro6') DEFAULT NULL,
  `abs` enum('Da','Nu') DEFAULT NULL,
  `alte_informatii` varchar(255) DEFAULT NULL,
  `Id` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masina`
--

INSERT INTO `masina` (`Id_masina`, `model_masina`, `an_fabricatie`, `km_parcursi`, `pret`, `cutie_viteze`, `carte_service`, `clasa_emisii`, `abs`, `alte_informatii`, `Id`) VALUES
(7, 'Audi A3', 2008, 11111, 2222, 'Manuala', 'Da', 'Euro2', 'Da', 'A3', 9),
(8, 'BMW 320d', 2012, 888, 777, 'Automata', 'Nu', 'Euro2', 'Da', 'Seria 3', 9),
(9, 'Audi A4', 2007, 666, 555, 'Automata', 'Da', 'Euro2', 'Da', 'A4', 8),
(10, 'Opel Astra GTC', 2009, 55556, 7890, 'Manuala', 'Da', 'Euro4', 'Da', 'GTC', 8);

-- --------------------------------------------------------

--
-- Table structure for table `poze`
--

CREATE TABLE `poze` (
  `Id_poza` int(3) NOT NULL,
  `poza_masina` varchar(255) DEFAULT NULL,
  `Id_masina` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poze`
--

INSERT INTO `poze` (`Id_poza`, `poza_masina`, `Id_masina`) VALUES
(11, '1.JPG', 7),
(12, '2.JPG', 8),
(13, 'A4.JPG', 9),
(14, '1.jpg', 10);

-- --------------------------------------------------------

--
-- Table structure for table `vanzatori`
--

CREATE TABLE `vanzatori` (
  `Id` int(3) NOT NULL,
  `nume` varchar(255) DEFAULT NULL,
  `telefon` bigint(14) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tip_vanzator` enum('Dealer','Persoana_privata') DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `parola` char(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vanzatori`
--

INSERT INTO `vanzatori` (`Id`, `nume`, `telefon`, `email`, `tip_vanzator`, `username`, `parola`) VALUES
(8, 'Sava Mircea', 745765922, 'mircea.sava@gmail.com', 'Persoana_privata', 'mircea', '$2y$10$UEFncBd0ze746IsX.Y5.uu0jMT0UQ7nevxbuGaw6Pu5cWK6SaxkTq'),
(9, 'Gabi Campion', 744500053, 'gabi.campion@gmail.com', 'Dealer', 'gabi', '$2y$10$BA8E2Gt4CRSPvGRgvsuDp..57Hpvtt.I/4PUUGR7FT8kb6RbsiZTO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `masina`
--
ALTER TABLE `masina`
  ADD PRIMARY KEY (`Id_masina`),
  ADD KEY `Id` (`Id`);

--
-- Indexes for table `poze`
--
ALTER TABLE `poze`
  ADD PRIMARY KEY (`Id_poza`),
  ADD KEY `Id_masina` (`Id_masina`);

--
-- Indexes for table `vanzatori`
--
ALTER TABLE `vanzatori`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `masina`
--
ALTER TABLE `masina`
  MODIFY `Id_masina` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `poze`
--
ALTER TABLE `poze`
  MODIFY `Id_poza` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `vanzatori`
--
ALTER TABLE `vanzatori`
  MODIFY `Id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `masina`
--
ALTER TABLE `masina`
  ADD CONSTRAINT `masina_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `vanzatori` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `poze`
--
ALTER TABLE `poze`
  ADD CONSTRAINT `poze_ibfk_1` FOREIGN KEY (`Id_masina`) REFERENCES `masina` (`Id_masina`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
