<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AUTOMAG-Pagina delete</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/slider.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="wrapper1">	
  <div class="content">
    <div class="column full-width-register1">
  	 <?php
	if(isset($_POST['Id_masina'])){
	 
       require_once("mysql_connect.php");
	   $query = "SELECT masina.*,poze.poza_masina FROM masina LEFT JOIN poze ON masina.Id_masina = poze.Id_masina WHERE masina.Id = {$_SESSION['Id']}" ;
	   $result = mysqli_query($link,$query) or die(mysqli_error($link));
	   $row = mysqli_fetch_row($result);
	   $g = $row[0];
	   $desired_dir="uploads/".$g."/";
	  
	   if(is_dir($desired_dir)==true){
  			foreach (glob($desired_dir."/*.*") as $filename) {
    				if (is_file($filename)) {
       					 unlink($filename);
      				}
   			}
   			rmdir($desired_dir); 
	    }
		
	    $query2 = "DELETE FROM masina WHERE Id_masina = $g AND Id = {$_SESSION['Id']}";
		$result2 = mysqli_query($link,$query2) or die(mysqli_error($link));
		if(mysqli_affected_rows($result2)>0){
			$_SESSION['mesaj'] = "Anuntul a fost sters!";	
		}
		else{
			$_SESSION['mesaj'] = "Anuntul nu a fost sters!";	
		}
		header("Location:my-account.php");
	}
   ?>	 
    </div>
  </div> 
</body>
</html>
