<?php
session_start();
if(!isset($_SESSION['Id'])){
	header("Location: login.php");
	die();
}
	if(isset($_POST['add'])){
		require_once('mysql_connect.php');	
		$model_masina = trim(mysqli_real_escape_string($link,$_POST['model_masina']));
		$an = $_POST['an'];
		$km_parcursi = mysqli_real_escape_string($link,$_POST['km_parcursi']);
		$pret = mysqli_real_escape_string($link,$_POST['pret']);
		$cutie_viteze = $_POST['cutie_viteze']; 
		
		if(isset($_POST['carte_service'])){
			$carte_service = $_POST['carte_service'];
		}
	    else{
		    $carte_service = "Nu";
	    }
		
		$clasa_emisii = $_POST['clasa_emisii'];
		
		if(isset($_POST['abs'])){
		   $abs = $_POST['abs'];
		}
	    else{
		    $abs = "Nu";
	    }
		
		$alte_informatii = $_POST['alte_informatii'];
		
		if (empty($model_masina)){
      		$errors[] = "Modelul masinii nu poate fi gol";
    	}
		
		if (empty($an)){
           $errors[] = "Completati anul de fabricatie!";	  
        }
		
		if ((empty($km_parcursi)) || (!filter_var($km_parcursi, FILTER_VALIDATE_INT))){
      		$errors[] = "Completati numarul de kilometri!";	  
    	}
		
		if ((empty($pret)) || (!filter_var($pret, FILTER_VALIDATE_INT))){
      		$errors[] = "Completati pretul masinii!";	  
    	}
		
		
		if (empty($cutie_viteze)){
           $errors[] = "Alegeti tipul cutiei de viteze!";	  
        }
		
		if (empty($clasa_emisii)){
           $errors[] = "Selectati clsa de emisii!";	  
        }
		
		if(!isset($errors)){
			$query = "INSERT INTO masina VALUES   (NULL,'$model_masina','$an','$km_parcursi','$pret','$cutie_viteze','$carte_service','$clasa_emisii','$abs','$alte_informatii',{$_SESSION['Id']})";
	   mysqli_query($link,$query);
		}
		
		//PRELUARE Id_masina DIN TABELUL masina
		
		$query2 = "SELECT LAST_INSERT_ID()";
		$result2 = mysqli_query($link,$query2);
		$row2 = mysqli_fetch_row($result2);
		$g = $row2[0];
		
		// VERIFICARE ADAUGARE POZE MASINA
		
		
		       $file_name = $_FILES['fisier']['name'];
		       $file_size = $_FILES['fisier']['size'];
		       $file_tmp = $_FILES['fisier']['tmp_name'];
		       $file_type= $_FILES['fisier']['type'];
			   
			  		   
			   $query3 = "INSERT INTO poze VALUES (NULL,'$file_name',$g)";
	  	       $result3 = mysqli_query($link,$query3);
			   
			   $desired_dir="uploads/".$g."/";
			   
			   if(empty($errors)==true){
            	if(is_dir($desired_dir)==false){
                	mkdir("uploads/".$g."/", 0700);		
            	}
				if(is_dir("uploads/".$_SESSION['username']."/".$file_name)==false){
                	move_uploaded_file($file_tmp,"uploads/".$g."/".$file_name);
				}				           
			}
			   
		
		mysqli_query($link,$query4);
        if (mysqli_affected_rows($link) >0){
            $succes = "Oferta a fost adaugata!";
            header("Location:my-account.php");
		    die();
		}
		else{
			$errors[] = "Oferta de vanzare nu a fost adaugata!";
		}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AUTOMAG-ADD OFFER</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="wrapper">
	<div class="logo">
		<a href="index.php"><img src="images/logo.jpg" width="350" height="127" /></a>        
    </div>
    <h1 class="text-logo">AUTOMAG</h1>
  <div class="clear"></div>
  <?php include "includes/header1.php";?>
  <div class="clear"></div>
  <div class="content">
    <div class="column full-width-register">
  	   <h1>Adaugare Masina</h1>
       <?php
		// afisez erorile sau mesajul de succes
		if (isset($errors)){
  			echo "\t\t<div class=\"error\">\n";
 			 foreach($errors as $error){
   			 echo "\t\t\t<p>$error</p>\n"; 
  			 }
  		echo "\t\t</div>\n";  
       }
       if (isset($succes)){
          echo "\t\t<p class=\"succes\">$succes</p>\n";
      }
	  if(isset($succes)){
		echo "\t\t<div class=\"succes1\">$succes1</div>\n";  
	  }
	  if (isset($_SESSION['succes'])){
  		  echo "\t\t<p class=\"succes\">{$_SESSION['succes']}</p>\n";
          unset($_SESSION['succes']);
      }
      ?> 
       <form action="add.php" method="POST" enctype="multipart/form-data">
       		<fieldset>
            	<legend>Informatii Masina</legend><br />
                <table>
                	<tr>
                    	<td><label for="model_masina" id="model_masina">Model Masina</label></td>
                        <td><input type="text" name="model_masina" value="" id="model_masina" /></td>           
                    </tr>
                    <tr>
                    	<td><label for="An_fabricatie" id="an_fabricatie">Anul de fabricatie</label></td>
                        <td><select name="an" id="an">
                        		<option value="" selected="selected">(selectati anul)</option>
                                <?php
								$an = date('Y');								
								while($an >= 1960){
									echo"<option value=\"".$an."\">".$an."</option>\n";
									$an--;
								}
								?>
                        	</select>
                        </td>
                    </tr> 
                    <tr>
                    	<td><label for="km_parcursi" id="km_parcursi">Km parcursi</label></td>
                        <td><input type="text" name="km_parcursi" id="km_parcursi" /> </td>                     
                     </tr>
                     <tr>
                    	<td><label for="pret" id="pret">Pret masina(euro)</label></td>
                        <td><input type="text" name="pret" id="pret" /> </td>                     
                     </tr>
                     <tr>
                     	<td><label for="cutie_viteze" id="cutie_viteze">Cutie de viteze</label></td>
                        <td>
                        	<select name="cutie_viteze" id="cutie_viteze">
                        		<option value="" selected="selected">(selectati cutia)</option>
                                <option value="Manuala">Manuala</option>
                                <option value="Automata">Automata</option>
                        	</select>
                        </td>
                     </tr>
                     <tr>
                     	<td><label for="carte_service" id="carte_service">Carte de service</label></td>
                        <td><input type="checkbox" name="carte_service" value="Da" /></td>
                     </tr>
                     <tr>
                     	<td><label for="clasa_emisii" id="clasa_emisii">Clasa de emisii</label></td>
                        <td>
                        	<select name="clasa_emisii" id="clasa_emisii">
                        		<option value="" selected="selected">(selectati tipul de emisii)</option>
                                <option value="Euro1">Euro1</option>
                                <option value="Euro2">Euro2</option>
                                <option value="Euro3">Euro3</option>
                                <option value="Euro4">Euro4</option>
                                <option value="Euro5">Euro5</option> 
                        	</select>
                        </td>
                     </tr>
                    <tr>
                    	<td><label for="ABS" id="ABS">ABS</label></td>
                        <td><input type="checkbox" name="abs" value="Da" /></td>
                    </tr>
                    <tr>
                    	<td><label for="alte_informatii" id="alte_informatii">Alte informatii</label></td>
                        <td><textarea name="alte_informatii" id="alte_informatii"></textarea></td><br /> 
                    </tr>
                    <tr>      
                           <td><label for="file" id="file">Selectati poza &nbsp;(Dim < 1MB)&nbsp;</label></td>
                           <td><input type="file" name="fisier"  id="file"/></td>                        	
                    </tr>      
                    <tr>                    
                    	<td colspan="2">
                        	<input type="hidden" name="add" value="true">
                            <input type="submit" value="Adauga" class="shift" />
                        </td>
                   </tr> 
                </table>
            </fieldset><br />
            </form>           
    </div>
  </div>
  <!--<div class="sidebar">
       <!-- Continut sidebar -->

  <!--</div>-->
  <div class="clear"></div>
</div>
<div class="footer">
&copy;2017 - MIRCEA
</div>
</body>
</html>
