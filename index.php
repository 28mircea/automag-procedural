<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AUTOMAG-SEARCH</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="wrapper">
	<div class="logo">
		<a href="index.php"><img src="images/logo.jpg" width="350" height="127" /></a>        
    </div>
    <h1 class="text-logo">AUTOMAG</h1>
  <div class="clear"></div>
  <?php include "includes/header1.php";?>
  <div class="clear"></div>
  <div class="content">
    <div class="column full-width-register">
  	   <h1>Cautare Masina</h1>
       <?php
		// Codul de cautare a masinii
		//var_dump($_POST);
		if(isset($_POST['search'])){

			require_once('mysql_connect.php');
			$query = "SELECT m.Id_masina,m.model_masina,m.pret,m.km_parcursi,m.an_fabricatie,p.poza_masina,v.nume,v.telefon FROM masina m INNER JOIN poze p ON m.Id_masina = p.Id_masina INNER JOIN vanzatori v ON m.Id = v.Id";
			$where = null;
			
			if(!empty($_POST['model_masina'])){
				$model_masina = trim(mysqli_real_escape_string($link,$_POST['model_masina']));
				$cuvinte = explode(' ',$model_masina);
				if (!$where) {
					$where = " WHERE ";	
				}
				if (!empty($cuvinte)){
					$where .= "(";
				}	
				foreach($cuvinte as $cuvant){
					$where .= "model_masina LIKE '%$cuvant%' OR ";
				}
				$where = substr($where,0,-4);
				$where .= ")";
					
		   }
		   
		if((!empty($_POST['an_de'])) && (!empty($_POST['an_pana']))){
			$an_de = $_POST['an_de'];
			$an_pana = $_POST['an_pana'];	
			if (!$where) {
				$where = " WHERE ";	
			}
			else {
				$where .= " OR ";
			}		  
			$where .= "(an_fabricatie BETWEEN '{$_POST['an_de']}' AND '{$_POST['an_pana']}' ";
			$where .= ")";
		}
		
		if((!empty($_POST['km_parcursi_de'])) && (!empty($_POST['km_parcursi_pana']))){
			$km_parcursi_de = $_POST['km_parcursi_de'];
			$km_parcursi_pana = $_POST['km_parcursi_pana'];	
			if (!$where) {
				$where = " WHERE ";	
			}
			else {
				$where .= " OR ";
			}  
			$where .= "(km_parcursi BETWEEN '{$_POST['km_parcursi_de']}' AND '{$_POST['km_parcursi_pana']}' ";
			$where .= ")";
		}		
		
		if((!empty($_POST['pret_de'])) && (!empty($_POST['pret_pana']))){
			$pret_de = $_POST['pret_de'];
			$pret_pana = $_POST['pret_pana'];
			if(!$where){
				$where = " WHERE";
			}
			else{
				$where = " OR";
				}					  
			$where .= "(pret BETWEEN '{$_POST['pret_de']}' AND '{$_POST['pret_pana']}' ";
			$where .= ")";
		}		
		
		if(!empty($_POST['cutie_viteze'])){
			$cutie_viteze = $_POST['cutie_viteze'];
			if (!$where) {
				$where = " WHERE ";	
			}
			else {
				$where .= " OR ";
			}			
			$where .= "(cutie_viteze ='{$_POST['cutie_viteze']}' ";
			$where .= ")";
		}
		
		
		if(isset($_POST['carte_service'])){
			$carte_service = 'Da';
			if (!$where) {
				$where = " WHERE ";	
			}
			else {
				$where .= " OR ";
			}
					  
			$where .= "(carte_service ='{$_POST['carte_service']}' ";
			$where .= ")";
		}
		
		
		if(!empty($_POST['clasa_emisii'])){
			$clasa_emisii = $_POST['clasa_emisii'];
			if (!$where) {
				$where = " WHERE ";	
			}
			else {
				$where .= " OR ";
			}			  
			$where .= "(clasa_emisii ='{$_POST['clasa_emisii']}' ";
			$where .= ")";
		}
		
		if(isset($_POST['abs'])){
			$abs = 'Da';
			if(!$where) {
				$where = " WHERE ";
			}
			else{
				$where .= " OR ";	
			}
			
			$where .= "(abs ='{$_POST['abs']}' ";
			$where .= ")";
		}
	
		$query .= $where . " ORDER BY pret DESC";
	    echo "<p>$query</p>";
		
		$result = mysqli_query($link,$query) or die(mysqli_error($link));
		if(mysqli_num_rows($result) > 0)
	      {
		    echo"<ul>\n";
			
           while($masina = mysqli_fetch_assoc($result))		
		         {
			    echo"\t<li>\n"; 
			    $g = $masina['Id_masina']; 
			  
         ?>
         	<div class="container">
        
  <div class="galerie" style="background-image: url('<?php echo"uploads/".$g."/".$masina['poza_masina'];?>'); background-size: 270px 250px;"></div>         
         <?php
		 		      echo"<div id=\"model-masina\">{$masina['model_masina']}</div>\n";	
                      echo"<div id=\"pret-masina\">{$masina['pret']} Euro</div>\n";
					  echo"<div id=\"km-parcursi\">Rulaj: {$masina['km_parcursi']} Km</div>\n";					  
					  echo"<div id=\"data-inmatriculare\">An fabricatie: {$masina['an_fabricatie']}</div>\n";
					  echo"<div id=\"nume-vanzator\">Vanzator: {$masina['nume']}</div>\n";
					  echo"<div id=\"telefon\">Tel.: {$masina['telefon']}</div>\n";
						          
				}//inchidere while($masina = mysqli_fetch_assoc($result))
		 ?> 
			   </div><!--inchidere div container-->
         <?php
		       echo"</li>";
			   echo"</ul>"; 
		 }//inchidere if(mysqli_num_rows($result) > 0)
		 
}//inchidere if(isset($_POST['search']))
else{
	 echo "<p>Nu s-a gasit nici o oferta care sa corespunda cerintelor dumneavoastra!</p>"; 
	 }
   ?> 
       <form action="index.php" method="POST">
       		<fieldset>
            	<legend>Informatii Cautare</legend><br />
                <table>
                	<tr>
                    	<td><label for="model_masina" id="model_masina">Model Masina</label></td>
                        <td><input type="text" name="model_masina" value="" id="search_model_masina" /></td>           
                    </tr>
                    <tr>
                    	<td><label for="An_fabricatie" id="an_fabricatie">Anul de fabricatie:</label></td>
                        <td><select name="an_de" id="search_an">
                        		<option value="" selected="selected">(de la:)</option>
                                <?php
								$an = date('Y');								
								while($an >= 1960){
									echo"<option value=\"".$an."\">".$an."</option>\n";
									$an--;
								}
								?>
                        	</select>
                      
                            <select name="an_pana" id="search_an">
                        		<option value="" selected="selected">(pana la:)</option>
                                <?php
								$an = date('Y');								
								while($an >= 1960){
									echo"<option value=\"".$an."\">".$an."</option>\n";
									$an--;
								}
								?>
                        	</select>
                       </td>
                    </tr> 
                    <tr>
                    	<td><label for="km_parcursi" id="km_parcursi">Km parcursi:</label></td>
                          <td><select name="km_parcursi_de" id="search_km_parcursi">
                        		<option value="" selected="selected">(de la:)</option>
                                <?php
								$km_parcursi = 1;								
								while($km_parcursi <= 350000){
									echo"<option value=\"".$km_parcursi."\">".$km_parcursi."</option>\n";
									$km_parcursi+=50000;
								}
								?>
                        	</select>
                              <select name="km_parcursi_pana" id="search_km_parcursi">
                        		<option value="" selected="selected">(pana la:)</option>
                                <?php
								$km_parcursi = 1;								
								while($km_parcursi <= 350000){
									echo"<option value=\"".$km_parcursi."\">".$km_parcursi."</option>\n";
									$km_parcursi+=50000;
								}
								?>
                        	</select>
                        </td>               
                     </tr>
                     <tr>
                    	<td><label for="pret" id="pret">Pret masina(euro):</label></td>
                        <td><select name="pret_de" id="search_pret">
                        		<option value="" selected="selected">(de la:)</option>
                                <?php
								$pret = 1;								
								while($pret_de <= 50000){
									echo"<option value=\"".$pret_de."\">".$pret_de."</option>\n";
									$pret_de+=3000;
								}
								?>
                        	</select>
                            <select name="pret_pana" id="search_pret">
                        		<option value="" selected="selected">(pana la:)</option>
                                <?php
								$pret = 1;								
								while($pret_pana <= 50000){
									echo"<option value=\"".$pret_pana."\">".$pret_pana."</option>\n";
									$pret_pana+=3000;
								}
								?>
                        	</select>
                        </td>                             
                  </tr>
                     <tr>
                     	<td><label for="cutie_viteze" id="cutie_viteze">Cutie de viteze</label></td>
                        <td>
                        	<select name="cutie_viteze" id="cutie_viteze">
                        		<option value="" selected="selected">(selectati cutia)</option>
                                <option value="Manuala">Manuala</option>
                                <option value="Automata">Automata</option>
                        	</select>
                        </td>
                     </tr>
                     <tr>
                     	<td><label for="carte_service" id="carte_service">Carte de service</label></td>
                        <td><input type="checkbox" name="carte_service" value="Da" /></td>
                     </tr>
                     <tr>
                     	<td><label for="clasa_emisii" id="clasa_emisii">Clasa de emisii</label></td>
                        <td>
                        	<select name="clasa_emisii" id="clasa_emisii">
                        		<option value="" selected="selected">(selectati tipul de emisii)</option>
                                <option value="Euro1">Euro1</option>
                                <option value="Euro2">Euro2</option>
                                <option value="Euro3">Euro3</option>
                                <option value="Euro4">Euro4</option>
                                <option value="Euro5">Euro5</option> 
                        	</select>
                        </td>
                     </tr>
                    <tr>
                    	<td><label for="ABS" id="abs">ABS</label></td>
                        <td><input type="checkbox" name="abs" value="Da" /></td>
                    </tr>
                    <tr>                    
                    	<td colspan="2">
                        	<input type="hidden" name="search" value="true">
                            <input type="submit" value="Search" class="shift" />
                        </td>
                   </tr> 
                </table>
            </fieldset><br />
            </form>           
    </div>
  </div>
 
<div class="clear"></div>
</div>
<div class="footer">
&copy;2017 - MIRCEA
</div>
</body>
</html>
