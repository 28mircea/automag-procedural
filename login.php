<?php
session_start();
if(isset($_POST['login'])){
	require_once('mysql_connect.php');
	$username = mysqli_real_escape_string($link,$_POST['username']);
	$parola = $_POST['parola'];
	//Se verifica existanta adresei de email
	if((empty($username)) || (strpos($username," "))){
		$errors[] = "Adaugati username";	
	}
	else{
		$query = "SELECT * FROM vanzatori WHERE username = '$username'";
		$result = mysqli_query($link,$query);
		if($usr = mysqli_fetch_assoc($result)){
		   $hash = $usr['parola'];
		   if(password_verify($parola,$hash)){
		   		$succes = "Bine ai venit {$usr['username']}!";
				$_SESSION['Id'] = $usr['Id'];
				$_SESSION['username'] = $usr['username'];
				header("Location:my-account.php");
		   }
		   else{
				$errors[] = "Parola incorecta!";   
		   }		   
		}
		else{
			$errors[] = "Username incorect!";	
		}
	}
}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AUTOMAG-Pagina login</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="wrapper">
	<div class="logo">
		<a href="index.php"><img src="images/logo.jpg" width="350" height="127" /></a>        
    </div>
    <h1 class="text-logo">AUTOMAG</h1>
  <div class="clear"></div>
  <?php include "includes/header1.php";?>
  <div class="clear"></div>
  <div class="content">
    <div class="column full-width-register">
  	   <h1>Autentificare Vanzator</h1>
       <?php
		// afisez erorile sau mesajul de succes
		if (isset($errors)){
  			echo "\t\t<div class=\"error\">\n";
 			 foreach($errors as $error){
   			 echo "\t\t\t<p>$error</p>\n"; 
  			 }
  		echo "\t\t</div>\n";  
       }
       if (isset($succes)){
          echo "\t\t<p class=\"succes\">$succes</p>\n";
      }
	  //if(isset($succes1)){
		//echo "\t\t<div class=\"succes1\">$succes1</div>\n";  
	  //}
      //?> 
       <form action="login.php" method="POST">
            <fieldset><br />
            	<legend>Informatii Logon</legend>
                <table>
                	<tr>
                    	<td><label for="username" id="username">Username*</label></td>
                        <td><input type="text" name="username" value="" id="username" /></td>
                    </tr>
                    <tr>
                    	<td><label for="parola" id="parola">Parola*</label></td>
                        <td><input type="password" name="parola" value="" id="parola" /></td>
                    </tr>                    
                    <tr>
                    	<td><input type="hidden" name="login" value="true"></td>
                        <td><input type="submit" value="Login" class="shift" /></td>                
                    </tr> 		
                </table>
            </fieldset>
	   </form>
       <p class="message">Nu aveti cont? Va puteti inregistra <a href="register.php" style="color:red">Aici</a></p>
    </div>
  </div>
  <!--<div class="sidebar">
       <!-- Continut sidebar -->

  <!--</div>-->
  <div class="clear"></div>
</div>
<div class="footer">
&copy;2017 - MIRCEA
</div>
</body>
</html>
